# Use this image instead of official one to support ARM arch
FROM docker.io/lukechannings/deno:latest

# Copy code and prepare workdir
WORKDIR /bot
RUN chown -R deno:deno /bot

# Do not use root
USER deno

# Deps first so no recaching them each time
COPY ./deps.ts ./
COPY deno.jsonc ./

# Cache dependencies
RUN deno task cachedeps

# Copy rest of code
COPY *.ts ./
COPY ./src ./src

USER root
RUN chown -R deno:deno /bot
# Temporarily freeze deno version
#ENV DEBIAN_FRONTEND=noninteractive
#RUN apt update && apt install -y unzip && rm -rf /var/lib/apt/lists/*
#RUN deno upgrade --version 1.35.0
USER deno


CMD ["task","start"]

# Multiarch build: docker buildx build -f ./Dockerfile --platform linux/arm64,linux/amd64 -t name:tag --push .