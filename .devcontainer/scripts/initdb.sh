#!/usr/bin/env bash
# Connect to main database to create new db and roles
psql "postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB?sslmode=disable" <<-EOSQL
CREATE DATABASE $BOT_DB;
CREATE ROLE $BOT_USER WITH LOGIN PASSWORD '$BOT_PASSWORD';
EOSQL

# Connect to bot database so we can use grant on it
psql "postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$BOT_DB?sslmode=disable" <<-EOSQL
GRANT ALL ON DATABASE $BOT_DB TO $BOT_USER;
GRANT ALL ON SCHEMA public TO $BOT_USER;
EOSQL