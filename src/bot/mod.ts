import {
  collectorsHandleInteractionCreate,
  convertCommands,
  fastFileLoader,
  initCollectors,
  TeamMembershipStates,
} from "../../deps.ts";
import { connectToDB } from "../db/mod.ts";

import env from "../utils/env.ts";
import { applyFixes } from "../utils/fixes.ts";
import { enableInfluxMetrics } from "../utils/influx.ts";
import { logger } from "../utils/logger.ts";
import { CommandsStore } from "./commands/mod.ts";
import { bot, fileLoaderExtensions, fileLoaderPaths } from "./core.ts";
import { startCron } from "./cron/mod.ts";
import { setupCommandEvents } from "./events/commands.ts";
import { setupDiscordEvents } from "./events/mod.ts";
import { owners } from "./inhibitors/owner.ts";

await fastFileLoader(fileLoaderPaths, fileLoaderExtensions);

// Discordeno fixes
applyFixes();

// Setup events
setupDiscordEvents();
setupCommandEvents();
bot.eventEmitters.once("ready", (data) => logger.info(`Bot: ${data.user.tag}`));
bot.eventEmitters.on(
  "ready",
  (data) => logger.info(`Ready ${data.shard?.[0]}/${data.shard?.[1]}`),
);
//

// Connect to database
await connectToDB();

// Influx metrics
if (env.influxenabled) enableInfluxMetrics();

// Get owners
logger.debug("Getting app info");

const appInfo = await bot.rest.getApplicationInfo();

if (appInfo.team) {
  appInfo.team.members.forEach((m) => {
    if (m.membershipState == TeamMembershipStates.Accepted) {
      owners.add(bot.transformers.snowflake(m.user.id));
    }
  });
} else if (appInfo.owner?.id) owners.add(bot.transformers.snowflake(appInfo.owner.id));

// Prepare collectors
bot.eventEmitters.on("interactionCreate", (data) => collectorsHandleInteractionCreate(bot.rest, data));
initCollectors();

logger.info("Starting...");

await bot.start();

if (env.testguildid) {
  await bot.rest.upsertGuildApplicationCommands(
    env.testguildid,
    convertCommands(CommandsStore),
  );
}

// Start cron
startCron();
