import env from "../../utils/env.ts";
import { logger } from "../../utils/logger.ts";
import { bot } from "../core.ts";

export function setupCommandEvents() {
  bot.commandEvents.on("trigger", (data) => {
    if (data.type === "slashautocomplete") return;
    logger.info("CMD Trigger", data.type, data.name);
  });

  bot.commandEvents.on("inhibited", (data) => {
    if (data.type === "slashautocomplete") return;
    logger.info("CMD Inhibited", data.type, data.name);
  });

  bot.commandEvents.on("error", async (data) => {
    logger.error(
      "CMD Error",
      data.type,
      data.name,
      Deno.inspect(data.error, { depth: 10, colors: true }),
      data.error.cause,
    );

    const response = env.dev
      ? (() => {
        const err = Deno.inspect({ error: data.error, cause: data.error.cause }, { colors: false, depth: 10 });
        return {
          content: err.length > 4086 ? undefined : [
            "```ts",
            err,
            "```",
          ].join("\n"),
          files: err.length > 4086
            ? [{
              name: "error.ts",
              blob: new Blob([err]),
            }]
            : undefined,
        };
      })()
      : {
        content: "Something went wrong",
        flags: 64,
      };

    if (data.interactionWasRespondedTo) {
      await bot.rest.sendFollowupMessage(data.data.token, response).catch(logger.error);
    } else {
      await bot.rest.sendInteractionResponse(data.data.id, data.data.token, {
        type: 4,
        data: response,
      }).catch(logger.error);
    }
  });

  bot.commandEvents.on("missing", (data) => {
    logger.warn("CMD Missing", data.type, data.name);
  });

  bot.commandEvents.on("completed", (data) => {
    if (data.type === "slashautocomplete") return;
    logger.info("CMD Completed", data.type, data.name);
  });
}
