import { convertCommands, DiscordUnavailableGuild } from "../../../deps.ts";
import { CommandVersion } from "../../db/commandVersion.ts";
import env from "../../utils/env.ts";
import { logger } from "../../utils/logger.ts";
import { CommandsStore } from "../commands/mod.ts";
import { bot } from "../core.ts";

export const slashcacheVersions = new Map<bigint, number>();

export default function () {
  bot.eventEmitters.on("raw", async (data) => {
    if (env.dev || !data.t) return;

    if (data.t === "GUILD_DELETE") {
      const id = bot.transformers.snowflake((data.d as DiscordUnavailableGuild).id);

      // delete command from db
      await CommandVersion.destroy({ where: { id } });
      // delete command from db cache
      slashcacheVersions.delete(id);
      return;
    }

    let id: string | undefined | bigint = (["GUILD_CREATE", "GUILD_UPDATE"].includes(data.t)
      // deno-lint-ignore no-explicit-any
      ? (data.d as any).id
      // deno-lint-ignore no-explicit-any
      : (data.d as any).guild_id ?? "") ?? "";

    // If no id cancel.
    if (!id) return;

    id = bot.transformers.snowflake(id);

    // If using latest cancel.
    let latest = slashcacheVersions.get(id) ??
      (await CommandVersion.findOne({ attributes: ["version"], where: { id } }))
        ?.version;

    if (!latest) {
      await CommandVersion.create({ id, version: env.slashcommandversion });
      slashcacheVersions.set(id, env.slashcommandversion);
      latest = env.slashcommandversion;
    } else if (latest == env.slashcommandversion) return;

    // Ignore testing guild
    if (id == env.testguildid) return;

    // Completely New guild or some which is not using latest version
    logger.info(
      `[Slash Setup] Installing slash commands on Guild ${id} for Event ${data.t}`,
    );

    // Update guild command for this id
    await CommandVersion.upsert({
      id,
      version: env.slashcommandversion,
    });

    slashcacheVersions.set(id, env.slashcommandversion);

    await bot.rest.upsertGuildApplicationCommands(
      id,
      convertCommands(CommandsStore),
    );
  });
}
