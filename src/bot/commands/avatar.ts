import { type CommandExecutor, iconBigintToHash } from "../../../deps.ts";
import { CommandsStore } from "./mod.ts";

const fileSize = 2048;

CommandsStore.userContextCommands.avatar = function (): CommandExecutor {
  return (data) => {
    if (!data.data?.targetId) throw new Error("Could not find this user.");
    const user = data.data?.resolved?.users?.get(data.data?.targetId);
    const member = data?.data?.resolved?.members?.get(data.data?.targetId);

    if (!user) throw new Error("Could not find this user.");

    const fileExtension = iconBigintToHash((member?.avatar ? member.avatar : user.avatar)!).startsWith("a_")
      ? "gif"
      : "webp";

    const avatar = member?.avatar
      ? `https://cdn.discordapp.com/guilds/${data.guildId}/users/${user.id}/avatars/${member.avatar}.${fileExtension}?size=${fileSize}`
      : `https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.${fileExtension}?size=${fileSize}`;

    return {
      embeds: [{
        type: "rich",
        description: `User: <@${user.id}>\n[link](${avatar})`,
        image: { url: avatar },
      }],
      allowedMentions: {
        users: [],
      },
    };
  };
};
