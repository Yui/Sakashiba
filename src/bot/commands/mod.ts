import { type CommandsStore as cmdstoretype } from "../../../deps.ts";

export const CommandsStore = {
  slashCommands: {},
  messageContextCommamnds: {},
  userContextCommands: {},
} as cmdstoretype;
