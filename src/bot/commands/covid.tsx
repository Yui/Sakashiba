import { collectComponent } from "https://codeberg.org/Yui/katsura/raw/tag/1.3.12/addons/collectors/mod.ts";
import {
  Camelize,
  CommandExecutor,
  DiscordEmbed,
  diseaseshEnums,
  Interaction,
  Row,
  SelecMenu,
  useDescription,
  useSlashCommandOption,
} from "../../../deps.ts";
import { covid } from "../../utils/apis/covid.ts";
import { imagechart } from "../../utils/apis/imagechart.ts";
import { DiscordColors, Time } from "../../utils/constants.ts";
import { dateToDiscordTimestamp, filterArrayWithLimit } from "../../utils/helpers.ts";
import { assumeAs } from "../../utils/types.ts";
import { bot } from "../core.ts";
import { useCooldownInhibitor } from "../inhibitors/cooldown.ts";
import { CommandsStore } from "./mod.ts";

const convertedCountriesEnum: string[] = [];

for (const val in diseaseshEnums.countries) {
  convertedCountriesEnum.push(String(diseaseshEnums.countries[val]));
}

CommandsStore.slashCommands.covid = {
  stats: function (): CommandExecutor {
    useDescription("Covid stats of specific country.");

    const country = useSlashCommandOption({
      name: "country",
      description: "x",
      required: true,
      type: "String",
      extra: {
        autoComplete: (text) => {
          return filterArrayWithLimit(convertedCountriesEnum, (s) => s.includes(text), 20).map(
            (s) => {
              return { name: s, value: s };
            },
          );
        },
      },
    });

    return async function* (interaction) {
      if (!convertedCountriesEnum.includes(country)) {
        return yield {
          content: `\`${country.replace("`", "'")}\` is not a valid country.`,
        };
      }

      assumeAs<keyof typeof diseaseshEnums.countries>(country);

      yield {
        type: "DeferredChannelMessageWithSource",
      };

      const data = await covid.country(country);

      const nf = Intl.NumberFormat(interaction.guildLocale);

      yield {
        embeds: [{
          title: "Covid 19 statistics",
          description: `Last update: ${
            dateToDiscordTimestamp(
              Math.round(data.updated / 1000),
              Date.now() - new Date(data.updated).getTime() > Time.day ? "Short Date" : "Relative Time",
            )
          }`,
          thumbnail: { url: data.countryInfo.flag },
          fields: [
            { name: "Total Cases", value: nf.format(data.cases), inline: true },
            { name: "Total Deaths", value: nf.format(data.deaths), inline: true },
            { name: "Total Recoveries", value: nf.format(data.recovered), inline: true },
            { name: "Total Tests", value: nf.format(data.deaths), inline: true },
            { name: "Active", value: nf.format(data.active), inline: true },
            { name: "Critical State", value: nf.format(data.critical), inline: true },
            { name: "Cases Today", value: nf.format(data.todayCases), inline: true },
            { name: "Deaths Today", value: nf.format(data.todayDeaths), inline: true },
            { name: "Recoveries Today", value: nf.format(data.todayRecovered), inline: true },
          ],
          color: DiscordColors.green.int,
        }],
      };
    };
  },
  graph: function (): CommandExecutor {
    useDescription("Graph visualisation");
    useCooldownInhibitor("covid-graph", Time.second * 5, { perUser: 1, global: 5 });

    const country = useSlashCommandOption({
      name: "country",
      description: "country to make graph for.",
      required: true,
      type: "String",
      extra: {
        autoComplete: (text) => {
          return filterArrayWithLimit(convertedCountriesEnum, (s) => s.includes(text), 20).map(
            (s) => {
              return { name: s, value: s };
            },
          );
        },
      },
    });

    const days = useSlashCommandOption({
      name: "days",
      description: "How many days to display.",
      defaultValue: 30,
      type: "Number",
    });

    let type = useSlashCommandOption({
      name: "type",
      description: "stat type",
      defaultValue: "cases",
      type: "String",
      extra: {
        choices: [{
          name: "Recovered",
          value: "recovered",
        }, {
          name: "Deaths",
          value: "deaths",
        }, {
          name: "Cases",
          value: "cases",
        }] as const,
      },
    });

    return async function* () {
      // TODO: make it possible to select more statistics to display via select menu (cases, deaths, etc)
      if (!convertedCountriesEnum.includes(country)) {
        return yield {
          content: "Unsupported country",
        };
      }

      assumeAs<keyof typeof diseaseshEnums.countries>(country);

      yield {
        type: "DeferredChannelMessageWithSource",
      };

      const data = await covid.countryHistory(country, days);

      let selectRes: Interaction | undefined;

      while (true) {
        const graphLabels: string[] = [];
        const graphPoints: number[] = [];

        // get first record
        let prevAmount: number | undefined;
        for (const k in data.timeline[type]) {
          prevAmount = data.timeline[type][k];
          delete data.timeline[type][k];
          break;
        }

        if (prevAmount == undefined) {
          yield {
            content: "Error:  invalid data",
            flags: 64,
          };
          if (selectRes) {
            await bot.rest.sendInteractionResponse(selectRes.id, selectRes.token, {
              type: 7,
              data: {
                components: [],
              },
            });
          }
          break;
        }

        for (const [date, amount] of Object.entries(data.timeline[type])) {
          graphLabels.push(date);
          graphPoints.push(amount - prevAmount);
        }

        const graphConfig: Parameters<typeof imagechart.graph>[1] = {
          width: 800,
          height: 800,
          bkg: "white",
          c: {
            type: "bar",
            data: {
              labels: graphLabels,
              datasets: [
                {
                  backgroundColor: "rgba(255,150,150,0.5)",
                  borderColor: "rgb(54,162,235)",
                  borderWidth: 1,
                  data: graphPoints,
                  label: "Covid history",
                },
              ],
            },
            options: {
              legend: {
                display: false,
              },
            },
          },
        };

        let graph: string | Blob | null = await imagechart.graph("link", graphConfig);

        // If graph is small enough let users call api themselves, if not get the image and send as attachment
        if (graph.length > 2048) {
          const res = await imagechart.graph("image", graphConfig).catch((err) => {
            if (err.message === "Rate limited") return null;
            else throw err;
          });
          if (res != null) graph = await res.blob();
          else return yield { content: "Cannot generate this graph at the moment. Try again later", flags: 64 };
        }

        const [selectProm, selectId] = collectComponent("component", { ttl: Time.minute * 2 });

        const embeds: Array<Camelize<DiscordEmbed>> = [{
          title: `Covid-19 Graph - ${data.country} ${type}`,
          description: `Last ${days} days`,
          image: typeof graph === "string"
            ? {
              url: graph,
            }
            : undefined,
        }];

        const components = [
          <Row>
            <SelecMenu
              label="a"
              customId={selectId}
              options={[{
                label: "Recovered",
                value: "recovered",
                default: type === "recovered",
              }, {
                label: "Deaths",
                value: "deaths",
                default: type === "deaths",
              }, {
                label: "Cases",
                value: "cases",
                default: type === "cases",
              }]}
            />
          </Row>,
        ];

        // Testing
        //if (typeof graph !== "string") await Deno.writeFile("./covid.png", new Uint8Array(await graph.arrayBuffer()));

        if (selectRes) {
          await bot.rest.sendInteractionResponse(selectRes.id, selectRes.token, {
            type: 7,
            data: {
              embeds,
              components,
              files: typeof graph === "string" ? undefined : [{
                name: "graph.png",
                blob: graph,
              }],
            },
          });
        } else {
          yield {
            type: "UpdateMessage",
            embeds,
            components,
            files: typeof graph === "string" ? undefined : [{
              name: "graph.png",
              blob: graph,
            }],
          };
        }

        selectRes = await selectProm;
        const [selectedOption] = selectRes.data?.values ?? [];
        if (!selectedOption) break;
        type = selectedOption as "cases" | "deaths" | "recovered";
      }
    };
  },
};
