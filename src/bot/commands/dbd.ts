import {
  type CommandExecutor,
  createCache,
  type dbdBetterPlayer,
  type dbdBetterShrine,
  type dbdPerk,
  dbdRanks,
  type LRU,
  useDescription,
  useSlashCommandOption,
} from "../../../deps.ts";
import { dbd } from "../../utils/apis/mod.ts";
import { DiscordColors, Regexes, Time } from "../../utils/constants.ts";
import { dateToDiscordTimestamp } from "../../utils/helpers.ts";
import { logger } from "../../utils/logger.ts";
import { CommandsStore } from "./mod.ts";

const shrineCache: LRU<string, dbdBetterShrine> = createCache(1);
const perksCache: LRU<string, dbdPerk> = createCache(4);
const playerCache: LRU<string, dbdBetterPlayer> = createCache(5);

async function getShrineData() {
  let data = shrineCache.get("shrine");
  if (data) return data;
  else {
    data = await dbd.shrine();
    shrineCache.set("shrine", data, data.end.getTime() - Date.now());
    return data;
  }
}

async function getPlayerData(steamId: string) {
  let data = playerCache.get(steamId);
  if (data) return data;

  data = await dbd.player(steamId);
  playerCache.set(steamId, data, Time.hour);
  return data;
}

async function getPerkData(id: string) {
  let data = perksCache.get(id);
  if (data) return data;

  data = await dbd.perk(id);
  const currentShrineEnd = shrineCache.get("shrine")?.end.getTime();
  perksCache.set(id, data, currentShrineEnd ? currentShrineEnd - Date.now() : Time.day);
  return data;
}

CommandsStore.slashCommands.dbd = {
  shrine: function (): CommandExecutor {
    useDescription("Shows Shrine of Secrets");

    return async function* () {
      yield {
        type: "DeferredChannelMessageWithSource",
      };

      const shrine = await getShrineData();
      const perks = await Promise.all(
        shrine.perks.map(async (perk) =>
          [perk, await getPerkData(perk.id)] as [dbdBetterShrine["perks"][number], dbdPerk]
        ),
      );
      const perkText = perks.map((perk) => `**${perk[1].name}** | ${perk[0].shards} Shards`).join("\n");

      yield {
        embeds: [{
          type: "rich",
          title: "Shrine",
          fields: [{
            name: "Start",
            value: dateToDiscordTimestamp(
              shrine.start,
              Math.abs(Date.now() - shrine.start.getTime()) > Time.day ? "Long Date/Time" : "Relative Time",
            ),
          }, {
            name: "End",
            value: dateToDiscordTimestamp(
              shrine.end,
              Math.abs(Date.now() - shrine.end.getTime()) > Time.day ? "Long Date/Time" : "Relative Time",
            ),
          }],
          description: perkText,
        }],
      };
    };
  },
  player: function (): CommandExecutor {
    // TODO: Figure out why the APi is still blocking me
    useDescription("Shows statistic of a player");

    const steamId = useSlashCommandOption({
      type: "String",
      required: true,
      name: "id",
      description: "Steam 64 id of the user.",
    });

    return async function* () {
      yield {
        type: "DeferredChannelMessageWithSource",
      };

      if (!Regexes.steamId64.exec(steamId.toString())) {
        yield {
          content: "Invalid steam id",
        };
        return;
      }

      const player = await getPlayerData(steamId).catch((err) => void logger.error(err));

      if (!player) {
        yield {
          content: "Could not reach the api.",
        };
        return;
      }

      yield {
        embeds: [{
          type: "rich",
          author: {
            name: "DBD User Statistics",
            url: `https://steamcommunity.com/profiles/${player.steamid}`,
          },
          color: DiscordColors.darkRed.int,
          fields: [{
            name: "Bloodpoints earned",
            value: player.bloodpoints.toLocaleString(),
            inline: true,
          }, {
            name: "Play time",
            value: `${(player.playtime / 60).toFixed(0)} Hours`, // TODO: Maybe add full time too?
            inline: true,
          }, {
            name: "Survivor rank",
            value: `${dbdRanks[player.survivor_rank.rank]} ${player.survivor_rank.tier}`,
            inline: true,
          }, {
            name: "Generators repaired",
            value: player.gensrepaired.toLocaleString(),
            inline: true,
          }, {
            name: "Trials escaped hatch|all",
            value: `${player.escaped_hatch.toLocaleString()}|${player.escaped}`,
            inline: true,
          }, {
            name: "Items depleted",
            value: player.itemsdepleted.toLocaleString(),
            inline: true,
          }, {
            name: "Killer Rank",
            value: `${dbdRanks[player.killer_rank.rank]} ${player.killer_rank.tier}`,
            inline: true,
          }, {
            name: "Sacrificied|Killed",
            value: `${player.sacrificed.toLocaleString()}|${player.killed.toLocaleString()}`,
            inline: true,
          }, {
            name: "Hatch escapes|closed",
            value: `${player.escaped_hatch.toLocaleString()}|${player.hatchesclosed.toLocaleString()}`,
            inline: true,
          }, {
            name: "Ultra rare offering burned survivor|killer",
            value: `${player.survivor_ultrarare.toLocaleString()}|${player.killer_ultrarare.toLocaleString()}`,
          }],
          footer: { text: player.steamid.toString() },
        }],
      };
    };
  },
};
