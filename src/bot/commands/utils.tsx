import {
  collectComponent,
  type CommandExecutor,
  InputText,
  minitz,
  Op,
  qrcode,
  Row,
  useDescription,
  useSlashCommandOption,
} from "../../../deps.ts";
import { Note, NoteSizeLimit } from "../../db/notes.ts";
import { Reminder, ReminderSizeLimit } from "../../db/reminders.ts";
import { DiscordColors, Regexes, Time } from "../../utils/constants.ts";
import { dateToDiscordTimestamp, snowflakeToTimestamp } from "../../utils/helpers.ts";
import { logger } from "../../utils/logger.ts";
import { createDynamicPagination } from "../../utils/pagination.tsx";
import { CommandsStore } from "./mod.ts";

CommandsStore.slashCommands.utils = {
  qr: function (): CommandExecutor {
    useDescription("Generates a QR Code");

    const text = useSlashCommandOption({
      type: "String",
      required: true,
      name: "text",
      description: "text to encode",
    });

    const hidden = useSlashCommandOption({
      type: "Boolean",
      defaultValue: false,
      name: "hidden",
      description: "Use ephermal response",
    });

    return async function () {
      const base64Image = String(await qrcode(text, { size: 500 }));

      const converted = await (await fetch(base64Image)).blob();
      //await Deno.writeFile("./qr.png", new Uint8Array(await converted.arrayBuffer()));

      return {
        files: [{
          name: "qr.png",
          blob: converted,
        }],
        flags: hidden ? 64 : undefined,
      };
    };
  },
  w2g: function (): CommandExecutor {
    useDescription("Create a room to watch videos on w2g.tv.");

    const video = useSlashCommandOption({
      name: "video",
      description: "Video to load.",
      type: "String",
    });

    return async function* () {
      yield {
        type: "DeferredChannelMessageWithSource",
      };

      // Create the room
      const createRes = await fetch("https://api.w2g.tv/rooms/create.json", {
        method: "POST",
      });

      // Make sure it went ok
      if (!createRes.ok || !createRes.headers.get("content-type")?.includes("application/json")) {
        throw new Error("API did not return valid data", { cause: createRes });
      }

      const createJson = await createRes.json();

      if (!createJson.streamkey) throw new Error("API did not return valid data", { cause: createJson });

      // Add the video if needed
      if (video) {
        const cookie = createRes.headers.get("set-cookie")?.split(";")?.[0];

        const addRes = await fetch(
          `https://api.w2g.tv/rooms/${createJson.streamkey}/sync_update.json`,
          {
            method: "POST",
            body: JSON.stringify({ "item_url": video }),
            headers: cookie ? new Headers({ "cookie": cookie, "Content-Type": "application/json" }) : undefined,
          },
        );

        // Make sure it went ok
        if (!addRes.ok) {
          throw new Error("API did not return valid data", { cause: addRes });
        }
      }

      yield {
        embeds: [{
          type: "rich",
          description: `[Your room](https://w2g.tv/rooms/${createJson.streamkey})`,
        }],
      };
    };
  },

  remind: {
    create: function (): CommandExecutor {
      useDescription("Creates a reminder. You will receive a message with your note on the specified time.");

      const time = useSlashCommandOption({
        name: "time",
        description: "dd:hh:mm:ss or a date `yyyy-mm-dd hh:mm` (You can use timezone with date).",
        type: "String",
        required: true,
      });

      const timezone = useSlashCommandOption({
        name: "timezone",
        description: "Timezone to use. Example: Europe/Berlin",
        type: "String",
        required: false,
      });

      let note = useSlashCommandOption({
        name: "note",
        description: "Note in the reminder",
        type: "String",
      });

      const hidden = useSlashCommandOption({
        type: "Boolean",
        name: "hidden",
        defaultValue: true,
        description: "Use ephermal response",
      });

      return async function* (data) {
        const flags = hidden ? 64 : undefined;
        const userId = data.member?.user?.id ?? data.user?.id;

        if (!userId) throw new Error("Could not parse user id", { cause: data });

        // Handle modal
        if (note) {
          yield {
            type: "DeferredChannelMessageWithSource",
            flags,
          };
        } else {
          const [modalPromise, customid] = collectComponent("modal");
          yield {
            type: "Modal",
            customId: customid,
            title: "Note",
            components: [
              <Row>
                <InputText customId="x" label="Note in the reminder" />
              </Row>,
            ],
          };

          const modal = await modalPromise.catch((err) => {
            logger.warn(err);
          });
          if (
            !modal || modal.data?.components?.[0]?.components?.[0]?.type != 4 ||
            !modal.data.components[0].components[0].value
          ) {
            yield {
              content: "No response in time",
              flags: 64,
            };
            return;
          }

          note = modal.data.components[0].components[0].value;
        }

        if (note.length > ReminderSizeLimit) {
          return yield {
            content: `Your note is ${note.length} characters long. It cannot be over ${ReminderSizeLimit}.`,
            flags: 64,
          };
        }

        let reminderTarget;

        if (timezone) {
          try {
            const convertedToLocalTime = minitz.fromTZISO(time, timezone);
            reminderTarget = new Date(convertedToLocalTime.getTime());
          } catch {
            return yield {
              content: "Invalid timezone",
              flags: 64,
            };
          }
        } else {
          const parsedDate = Date.parse(time);
          if (isNaN(parsedDate)) {
            let parsedDuration = 0;
            for (const [i, token] of time.split(":").reverse().entries()) {
              const parsedToken = Number(token);

              if (isNaN(parsedToken)) {
                yield {
                  content:
                    `\`${token}\` is not a number. Either specify the time via \`dd:hh:mm:ss\`\nExample for a reminder in 2 hours from now: \`2:0:0\`\nor use a date in GMT format.\nExample: \`04 Dec 1995 00:12:00 GMT\` or \`December 12, 1995 12:00 AM\``,
                  flags: 64,
                };
                return;
              }

              switch (i) {
                case 0: // seconds
                  parsedDuration += parsedToken * Time.second;
                  break;
                case 1: // minutes
                  parsedDuration += parsedToken * Time.minute;
                  break;
                case 2: // hours
                  parsedDuration += parsedToken * Time.hour;
                  break;
                case 3: // days
                  parsedDuration += parsedToken * Time.day;
                  break;
              }
            }
            reminderTarget = new Date(Date.now() + parsedDuration);
          } else reminderTarget = new Date(parsedDate);

          if (reminderTarget.getTime() < Date.now()) {
            return yield {
              content: "You cannot make a reminder to the past.",
              flags,
            };
          }
        }

        note = note.replaceAll("`", "'");

        // TODO: Make reminders use bigints
        const reminder = await Reminder.create({ userid: userId, note, date: reminderTarget });

        yield {
          embeds: [{
            type: "rich",
            description: `Will remind you at ${
              dateToDiscordTimestamp(reminderTarget, "Short Date/Time")
            } to \`${note}\``,
            fields: [{ name: "id", value: reminder.id?.toString() ?? "??" }],
            footer: { text: "This message contains user generated content." },
            color: DiscordColors.aqua.int,
          }],
          flags,
        };
      };
    },
    delete: function (): CommandExecutor {
      useDescription("Deletes a reminder. You can see the id in `/remind list`.");
      const id = useSlashCommandOption({
        name: "id",
        description: "id of the reminder",
        type: "String",
        required: true,
      });

      return async (data) => {
        const userId = data.member?.user?.id ?? data.user?.id;

        if (!userId) throw new Error("Could not parse user id", { cause: data });

        await Reminder.destroy({ where: { userid: userId, id }, limit: 1 });
        return {
          content: "Deleted",
        };
      };
    },
    info: function (): CommandExecutor {
      useDescription("Displays details about a specific reminder. You can see the id in `/remind list`.");

      const id = useSlashCommandOption({
        name: "id",
        description: "id of the reminder",
        type: "String",
        required: true,
      });

      const hidden = useSlashCommandOption({
        type: "Boolean",
        name: "hidden",
        defaultValue: true,
        description: "Use ephermal response",
      });

      return async (data) => {
        const userId = data.member?.user?.id ?? data.user?.id;

        if (!userId) throw new Error("Could not parse user id", { cause: data });

        const reminder = await Reminder.findOne({ where: { userid: userId, id } });
        const flags = hidden ? 64 : undefined;

        if (!reminder) {
          return {
            content: "You do not have any reminder with this id.",
            flags,
          };
        }

        return {
          embeds: [{
            type: "rich",
            title: `Reminder ${reminder.id}`,
            description: `\`${reminder.note}\``,
            fields: [{
              name: "date",
              value: dateToDiscordTimestamp(reminder.date, "Short Date/Time"),
            }],
            footer: { text: "This message contains user generated content." },
            color: DiscordColors.aqua.int,
          }],
          flags,
        };
      };
    },
    list: function (): CommandExecutor {
      useDescription("Lists your reminders.");

      const hidden = useSlashCommandOption({
        type: "Boolean",
        name: "hidden",
        defaultValue: true,
        description: "Use ephermal response",
      });

      return async function* (data) {
        const userId = data.member?.user?.id ?? data.user?.id;
        if (!userId) throw new Error("Could not parse user id", { cause: data });

        const flags = hidden ? 64 : undefined;

        yield {
          type: "DeferredChannelMessageWithSource",
          flags,
        };

        if (await Reminder.count({ where: { userid: userId } }) == 0) {
          return yield {
            content: "You do not have any reminders.",
            flags,
          };
        }

        // 3 per page
        const pagination = await createDynamicPagination({
          count: async () => Math.ceil(await Reminder.count({ where: { userid: userId } }) / 5),
          page: async (pageIndex, maxPages) => {
            const page = await Reminder.findAll({ where: { userid: userId }, offset: 5 * pageIndex, limit: 5 });
            return {
              embeds: [{
                type: "rich",
                title: "Your reminders",
                description: page.map((r) =>
                  `\`${r.id}\` ${dateToDiscordTimestamp(r.date, "Short Date/Time")} ${r.note}`
                ).join("\n"),
                footer: { text: `Page ${pageIndex + 1}/${maxPages}\nThis message contains user generated content.` },
                color: DiscordColors.aqua.int,
              }],
            };
          },
          ephermal: hidden,
        });

        yield pagination.data;

        await pagination.end.catch(logger.error);
      };
    },
  },

  urban: function (): CommandExecutor {
    useDescription("Finds a definition of a word in urban dictionary.");

    const term = useSlashCommandOption({
      name: "term",
      description: "term to lookup",
      type: "String",
      required: true,
    });

    return async function* () {
      yield {
        type: "DeferredChannelMessageWithSource",
        flags: 64,
      };

      const definitions = await fetch(
        `https://api.urbandictionary.com/v0/define?${new URLSearchParams({
          page: "1",
          term: term,
        })}`,
      ).then(
        async (r) => {
          if (!r.ok || !r.headers.get("content-type")?.includes("application/json")) {
            throw new Error("Invalid response");
          }

          const data = await r.json() as {
            list: {
              definition: string;
              permalink: string;
              thumbs_up: number;
              author: string;
              word: string;
              defid: number;
              current_vote: string;
              /** Date string */
              written_on: string;
              example: string;
              thumbs_down: number;
            }[];
          };
          return data.list;
        },
      );

      const pagination = await createDynamicPagination({
        count: () => definitions.length,
        page: (i, max) => {
          const def = definitions[i];

          return {
            embeds: {
              footer: {
                text: `Page ${i + 1}/${max} | This message contains user generated content.`,
              },
              description: def.definition,
              fields: [{ name: "term", value: term }, {
                name: "rating",
                value: `👍 ${def.thumbs_up} 👎 ${def.thumbs_down}`,
              }, {
                name: "written",
                value: dateToDiscordTimestamp(Math.round(Date.parse(def.written_on) / 1000), "Short Date/Time"),
              }],
              title: "Urban",
              color: DiscordColors.white.int,
            },
          };
        },
      });

      yield { ...pagination.data, flags: 64 };
    };
  },

  unix: function (): CommandExecutor {
    useDescription("Display a unix timestamp in discord timestamp format");

    const time = useSlashCommandOption({
      name: "timestamp",
      description: "The unix timestamp",
      type: "Number",
      required: true,
    });

    const format = useSlashCommandOption({
      name: "format",
      description: "format of the timestamp",
      type: "String",
      defaultValue: "Short Date",
      extra: {
        choices: [{
          name: "Short Time - 16:20",
          value: "Short Time",
        }, {
          name: "Long Time - 16:20:30",
          value: "Long Time",
        }, {
          name: "Short Date - 20/04/2021 ( Default format )",
          value: "Short Date",
        }, {
          name: "Long Date - 20 April 2021",
          value: "Long Date",
        }, {
          name: "Short Date/Time - 20 April 2021 16:20",
          value: "Short Date/Time",
        }, {
          name: "Long Date/Time - Tuesday, 20 April 2021 16:20",
          value: "Long Date/Time",
        }, {
          name: "Relative Time - x time ago",
          value: "Relative Time",
        }] as const,
      },
    });

    return () => {
      const timestamp = dateToDiscordTimestamp(time, format);
      return {
        content: `${timestamp} (\`${timestamp}\`)`,
      };
    };
  },

  snowflake: function (): CommandExecutor {
    useDescription("Provides info about discord snowflakes.");

    const snowflake = useSlashCommandOption({
      name: "snowflake",
      description: "x",
      type: "String",
      required: true,
    });

    return () => {
      if (!Regexes.snowflake.exec(snowflake)) {
        return {
          content: "Invalid snowflake",
        };
      }

      return {
        embeds: [{
          title: "Snowflake",
          description: `\`${snowflake}\``,
          fields: [{
            name: "created",
            value: `${dateToDiscordTimestamp(Math.round(snowflakeToTimestamp(snowflake) / 1000))} / ${
              dateToDiscordTimestamp(Math.round(snowflakeToTimestamp(snowflake) / 1000), "Relative Time")
            }`,
          }],
        }],
      };
    };
  },

  note: {
    create: function (): CommandExecutor {
      useDescription("Create a note with some text.");

      const text = useSlashCommandOption({
        name: "text",
        description: "snippet",
        type: "String",
        required: true,
      });

      const name = useSlashCommandOption({
        name: "name",
        description: "name of the snippet",
        type: "String",
        required: true,
      });

      return async function* (data) {
        const userId = data.member?.user?.id ?? data.user?.id;

        if (!userId) throw new Error("Could not parse user id", { cause: data });

        if (!data.guildId) {
          return yield {
            content: "You need to be in a guild.",
          };
        }

        if (text.length > NoteSizeLimit) {
          return yield {
            content: `Your note is ${text.length} characters long. It cannot be over ${NoteSizeLimit}.`,
            flags: 64,
          };
        }

        yield {
          type: "DeferredChannelMessageWithSource",
        };

        try {
          // TODO: Make notes use bigints
          await Note.create({ name, guildid: data.guildId, userid: userId, text });
        } catch (err) {
          if (err.name === "SequelizeUniqueConstraintError") {
            return yield {
              content: "Note with this name already exists.",
            };
          } else throw err;
        }

        yield {
          content: `Note with name \`${name}\` created.`,
        };
      };
    },
    delete: function (): CommandExecutor {
      useDescription("Deletes a note.");

      const name = useSlashCommandOption({
        name: "name",
        description: "name of the note",
        type: "String",
        required: true,
        extra: {
          autoComplete: async (value, _, data) => {
            if (!data.guildId) return [];

            // get from db
            const notes = await Note.findAll({
              where: {
                guildid: data.guildId,
                name: value === "" ? undefined : {
                  [Op.like]: `%${value}%`,
                },
              },
              limit: 20,
            });

            return notes.map((n) => {
              return {
                name: n.name,
                value: n.name,
              };
            });
          },
        },
      });

      return async function* (data) {
        if (!data.member) throw new Error("Member not found");

        yield {
          type: "DeferredChannelMessageWithSource",
          flags: 64,
        };

        const note = await Note.findOne({ where: { guildid: data.guildId, name } });

        if (!note) {
          return yield {
            content: `Note with name \`${name}\` not found.`,
            flags: 64,
          };
        }

        // TODO: Make notes use bigints
        if (note.userid !== data.member.user!.id) {
          const canRun = data.member?.permissions?.has("ADMINISTRATOR") ??
            data.member?.permissions?.has("MANAGE_MESSAGES") ?? false;

          if (!canRun) {
            return yield {
              content: "You cannot delete note from other users without having permission to Manage Messages.",
              flags: 64,
            };
          }
        }

        await Note.destroy({ where: { guildid: data.guildId, name }, limit: 1 });

        yield {
          content: "Note deleted",
        };
      };
    },
    show: function (): CommandExecutor {
      useDescription("Shows a note.");

      const name = useSlashCommandOption({
        name: "name",
        description: "name of the note",
        type: "String",
        required: true,
        extra: {
          autoComplete: async (value, _, data) => {
            if (!data.guildId) return [];

            // get from db
            const notes = await Note.findAll({
              where: {
                guildid: data.guildId,
                name: value === "" ? undefined : {
                  [Op.like]: `%${value}%`,
                },
              },
              limit: 20,
            });

            return notes.map((n) => {
              return {
                name: n.name,
                value: n.name,
              };
            });
          },
        },
      });

      return async function* (data) {
        if (!data.guildId) {
          return yield {
            content: "You need to be in a guild.",
          };
        }

        yield {
          type: "DeferredChannelMessageWithSource",
        };

        const note = await Note.findOne({ where: { name, guildid: data.guildId } });

        if (!note) {
          return yield {
            content: `Could not find note with name \`${name}\`.`,
            flags: 64,
          };
        }

        yield {
          embeds: [{
            type: "rich",
            description: note.text,
            fields: [{
              name: "name",
              value: name,
            }, {
              name: "Note made by",
              value: `<@${note.userid}> (\`${note.userid}\`)`,
            }],
            footer: { text: `This message contains user generated content.` },
          }],
          allowedMentions: {
            users: [],
          },
        };
      };
    },
    list: function (): CommandExecutor {
      useDescription("Displays notes in this server.");

      return async function* (data) {
        if (!data.guildId) {
          return yield {
            content: "You need to be in a guild.",
          };
        }

        yield {
          type: "DeferredChannelMessageWithSource",
        };

        if (await Note.count({ where: { guildid: data.guildId } }) == 0) {
          return yield {
            content: "This server does not have any notes.",
          };
        }

        // 5 per page

        const pagination = await createDynamicPagination({
          count: async () => Math.ceil(await Note.count({ where: { guildid: data.guildId } }) / 3),
          page: async (pageIndex, maxPages) => {
            const page = await Note.findAll({ where: { guildid: data.guildId }, offset: 3 * pageIndex, limit: 3 });
            return {
              embeds: [{
                type: "rich",
                title: "Notes in this server",
                description: page.map((n) => `\`${n.name}\` made by <@${n.userid}> (\`${n.userid}\`)`).join("\n"),
                footer: { text: `Page ${pageIndex + 1}/${maxPages}\nThis message contains user generated content.` },
                color: DiscordColors.aqua.int,
              }],
            };
          },
        });

        yield pagination.data;

        await pagination.end.catch(logger.error);
      };
    },
  },
};
