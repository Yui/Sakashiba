import { type CommandExecutor, DiscordenoVersion, useDescription } from "../../../deps.ts";
import { DiscordColors } from "../../utils/constants.ts";
import { dateToDiscordTimestamp, snowflakeToTimestamp } from "../../utils/helpers.ts";
import { CommandsStore } from "./mod.ts";

CommandsStore.slashCommands.botinfo = function (): CommandExecutor {
  useDescription("Show information about the bot.");

  return (data) => {
    const ping = Date.now() - snowflakeToTimestamp(data.id);

    const processStartedAt = Date.now() - performance.now();

    return {
      embeds: [{
        type: "rich",
        author: { name: "Info" }, // TODO: Add bot icon
        color: DiscordColors.blue.int,
        fields: [{
          name: "Ping",
          value: `${ping} ms`,
          inline: true,
        }, {
          name: "Running",
          value: dateToDiscordTimestamp(
            Math.round(processStartedAt / 1000),
            "Relative Time",
          ),
          inline: true,
        }, {
          name: "Runtime",
          value: `Deno v${Deno.version.deno}`,
        }, {
          name: "Library",
          value: `Discordeno v${DiscordenoVersion}`,
        }],
      }],
    };
  };
};
