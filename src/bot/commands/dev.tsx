import {
  Camelize,
  collectComponent,
  type CommandExecutor,
  DiscordEmbed,
  fastFileLoader,
  InputText,
  Row,
  useDescription,
  useSlashCommandOption,
} from "../../../deps.ts";
import { sequelize } from "../../db/mod.ts";
import { DiscordColors, SpecialChars } from "../../utils/constants.ts";
import { filterArrayWithLimit } from "../../utils/helpers.ts";
import { logger } from "../../utils/logger.ts";
import { bot, fileLoaderExtensions, fileLoaderPaths } from "../core.ts";
import { reloadCron } from "../cron/mod.ts";
import { useOwnerInhibitor } from "../inhibitors/mod.ts";
import { CommandsStore } from "./mod.ts";

const dbAutoCompleteOptions = [
  "listtables",
];

CommandsStore.slashCommands.dev = {
  reload: function (): CommandExecutor {
    useOwnerInhibitor();
    useDescription("Reloads commands");

    return async function* () {
      logger.warn("Reloading commands and cron jobs.");

      yield {
        type: "DeferredChannelMessageWithSource",
        flags: 64,
      };

      await fastFileLoader(fileLoaderPaths, fileLoaderExtensions);
      reloadCron();

      yield {
        content: "Reloaded",
        flags: 64,
      };
    };
  },
  eval: function (): CommandExecutor {
    useOwnerInhibitor();
    useDescription("Evaluates code");

    let code = useSlashCommandOption({
      type: "String",
      name: "code",
      description: "Code to evaluate",
      required: false,
    });

    const depth = useSlashCommandOption({
      type: "Number",
      name: "depth",
      defaultValue: 3,
      description: "Output inspection depth",
    });

    const hidden = useSlashCommandOption({
      type: "Boolean",
      name: "hidden",
      defaultValue: true,
      description: "Use ephermal response",
    });

    const long = useSlashCommandOption({
      type: "Boolean",
      name: "long",
      defaultValue: false,
      description: "Output will be sent as file if too long",
    });
    return async function* () {
      const flags = hidden ? 64 : undefined;

      if (code) {
        yield {
          type: "DeferredChannelMessageWithSource",
          flags,
        };
      } else {
        const [modalPromise, customid] = collectComponent("modal");
        yield {
          type: "Modal",
          customId: customid,
          title: "Code",
          components: [
            <Row>
              <InputText customId="x" label="Code to evaluate" />
            </Row>,
          ],
        };

        const modal = await modalPromise.catch((err) => {
          logger.warn(err);
        });
        if (
          !modal || modal.data?.components?.[0]?.components?.[0]?.type != 4 ||
          !modal.data.components[0].components[0].value
        ) {
          yield {
            content: "No response in time",
            flags: 64,
          };
          return;
        }

        code = modal.data.components[0].components[0].value;
      }

      let result,
        type,
        errorTriggered: boolean | undefined,
        start: number | undefined,
        stop: number | undefined;

      try {
        start = performance.now();
        result = eval(code);
        if (result instanceof Promise) result = await result;
        stop = performance.now();
        type = typeof result;
      } catch (error) {
        type = typeof error;
        result = error;
        errorTriggered = true;
      }

      // Format results
      let parsedResult = typeof result === "string" ? result : Deno.inspect(result, { depth: depth, showHidden: true });

      parsedResult = parsedResult.replaceAll(
        "```",
        `\`${SpecialChars.zws}`.repeat(3),
      ).replaceAll(bot.rest.token!.slice(4), "***");

      // Format input
      code = code.replaceAll("```", `\`${SpecialChars.zws}`.repeat(3));

      const useFile = long && parsedResult.length > 4086;

      // If not using file to send results than slice the string
      // embed desc max 4096 chars, with formatting stuff(-10) max --> 4086
      if (!useFile) {
        parsedResult.length > 4086 ? (parsedResult = `${parsedResult.substring(0, 4082)}\n...`) : result;
      }

      const embed: Camelize<DiscordEmbed> = {
        type: "rich",
        title: "Eval",
        fields: [{
          name: "Code",
          value: ["```ts", code, "```"].join("\n"),
        }, {
          name: "Type",
          value: type,
        }],
      };

      if (!useFile) {
        embed.description = ["```ts", parsedResult, "```"].join("\n");
      }

      if (stop && start) {
        const timeTaken = stop - start;
        embed.fields?.push({
          name: "Time taken",
          value: (timeTaken / 60000).toString(), // TODO: better formatting
        });
      }

      embed.color = errorTriggered ? DiscordColors.red.int : DiscordColors.green.int;

      yield {
        embeds: [embed],
        flags,
      };
    };
  },
  db: function (): CommandExecutor {
    useOwnerInhibitor();
    useDescription("Database tools");

    const query = useSlashCommandOption({
      name: "query",
      description: "database query",
      type: "String",
      required: true,
      extra: {
        autoComplete: (val) => {
          const data: { name: string; value: string }[] = filterArrayWithLimit(
            dbAutoCompleteOptions,
            (a) => a.includes(val),
            20,
          ).map((a) => ({ name: a, value: a }));

          return data;
        },
      },
    });

    const depth = useSlashCommandOption({
      type: "Number",
      name: "depth",
      defaultValue: 3,
      description: "Output inspection depth",
    });

    const hidden = useSlashCommandOption({
      type: "Boolean",
      name: "hidden",
      defaultValue: true,
      description: "Use ephermal response",
    });

    const long = useSlashCommandOption({
      type: "Boolean",
      name: "long",
      defaultValue: false,
      description: "Output will be sent as file if too long",
    });

    return async function* () {
      const flags = hidden ? 64 : undefined;

      yield {
        type: "DeferredChannelMessageWithSource",
        flags: flags,
      };

      // deno-lint-ignore no-explicit-any
      let result: any[] | Error,
        errorTriggered: boolean | undefined,
        start: number | undefined,
        stop: number | undefined,
        template: boolean | undefined;

      if (dbAutoCompleteOptions.includes(query)) {
        try {
          template = true;
          switch (query) {
            case "listtables": {
              start = performance.now();
              result = await sequelize.query(
                "SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';",
                {
                  nest: true,
                },
              ).then((r) =>
                r.map((x) =>
                  // deno-lint-ignore no-explicit-any
                  typeof x === "object" && Reflect.has(x as Record<any, any>, "tablename")
                    // deno-lint-ignore no-explicit-any
                    ? (x as Record<any, any>).tablename
                    : x
                )
              );
              stop = performance.now();
              break;
            }
            default:
              template = false;
              throw new Error(`Option '${query}' missing implementation.`, { cause: "missing" });
          }
        } catch (error) {
          if (error.cause === "missing") throw Error;
          result = error.message;
          errorTriggered = true;
        }
      } else {
        // run normal sql query
        try {
          start = performance.now();
          result = await sequelize.query(query, {
            nest: true,
          });
          stop = performance.now();
        } catch (error) {
          result = error.message;
          errorTriggered = true;
        }
      }

      // Format results
      let parsedResult = typeof result === "string" ? result : Deno.inspect(result, { depth: depth, showHidden: true });

      parsedResult = parsedResult.replaceAll(
        "```",
        `\`${SpecialChars.zws}`.repeat(3),
      ).replaceAll(bot.rest.token!.slice(4), "***");

      const useFile = long && parsedResult.length > 4086;

      // If not using file to send results than slice the string
      // embed desc max 4096 chars, with formatting stuff(-10) max --> 4086
      if (!useFile) {
        parsedResult.length > 4086 ? (parsedResult = `${parsedResult.substring(0, 4082)}\n...`) : result;
      }

      const embed: Camelize<DiscordEmbed> = {
        type: "rich",
        title: "db",
        fields: [],
      };

      if (template) embed.fields!.push({ name: "template", value: query });
      else {embed.fields!.push({
          name: "sql",
          value: ["```sql", query, "```"].join("\n"),
        });}

      if (!useFile) {
        embed.description = ["```ts", parsedResult, "```"].join("\n");
      }

      if (stop && start) {
        const timeTaken = stop - start;
        embed.fields?.push({
          name: "Time taken",
          value: (timeTaken / 60000).toString(), // TODO: better formatting
        });
      }

      embed.color = errorTriggered ? DiscordColors.red.int : DiscordColors.green.int;

      yield {
        embeds: [embed],
        flags,
        files: useFile
          ? [{
            name: "result.ts",
            blob: new Blob([parsedResult]),
          }]
          : undefined,
      };
    };
  },
};
