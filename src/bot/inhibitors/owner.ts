import type { inhibitor } from "../../../deps.ts";
import { useInhibitor } from "../../../deps.ts";

export const owners = new Set<bigint>();

const ownerInhibitor: inhibitor = (data) => {
  const id = data.member?.user?.id ?? data.user?.id;
  if (!id) return true;
  return !owners.has(id);
};

/** Only application owners will be able to use the command. */
export function useOwnerInhibitor() {
  useInhibitor(ownerInhibitor);
}
