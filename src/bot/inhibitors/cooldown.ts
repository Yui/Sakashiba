import { Interaction, InteractionTypes, useInhibitor } from "../../../deps.ts";
import { dateToDiscordTimestamp } from "../../utils/helpers.ts";
import { logger } from "../../utils/logger.ts";
import type { AtLeastOne } from "../../utils/types.ts";
import { bot } from "../core.ts";

interface cooldownStored {
  maxUses: number;
  uses: number;
  timestamp: number;
}

export const cooldowns = new Map<string, cooldownStored>();

const cooldownInhibitor = async (
  cooldownId: string,
  interaction: Interaction,
  cooldownOptions:
    & AtLeastOne<{
      global?: number;
      perUser?: number;
    }>
    & {
      time: number;
    },
) => {
  if (interaction.type !== InteractionTypes.ApplicationCommand) return false;

  if (cooldownOptions.global) {
    const key = `g-${cooldownId}`;
    const cd = cooldowns.get(key);

    if (cd) {
      if (cd.uses >= cd.maxUses) {
        if (cd.timestamp > Date.now()) {
          await bot.rest.sendInteractionResponse(interaction.id, interaction.token, {
            type: 4,
            data: {
              flags: 64,
              content: "This command cannot be used rn.",
            },
          }).catch(logger.error);
          return true;
        } else cd.uses = 0;
      }
    }

    cooldowns.set(key, {
      uses: (cd?.uses ?? 0) + 1,
      timestamp: Date.now() + cooldownOptions.time,
      maxUses: cooldownOptions.global,
    });
  }

  if (cooldownOptions.perUser) {
    const userId = interaction.member?.user?.id ?? interaction.user?.id;
    if (!userId) return true;

    const key = `u-${cooldownId}-${userId}`;
    const cd = cooldowns.get(key);

    if (cd) {
      if (cd.uses >= cd.maxUses) {
        if (cd.timestamp > Date.now()) {
          await bot.rest.sendInteractionResponse(interaction.id, interaction.token, {
            type: 4,
            data: {
              flags: 64,
              content: `You cannot use this command. Try again at ${
                dateToDiscordTimestamp(Math.round(cd.timestamp / 1000), "Relative Time")
              }`,
            },
          }).catch(logger.error);
          return true;
        } else cd.uses = 0;
      }
    }

    cooldowns.set(key, {
      uses: (cd?.uses ?? 0) + 1,
      timestamp: Date.now() + cooldownOptions.time,
      maxUses: cooldownOptions.perUser,
    });
  }

  return false;
};

/** Limits how often a command can run. */
export function useCooldownInhibitor(
  /** Unique id for this commamnd */
  id: string,
  /** Uses per this time. */
  time: number,
  config: AtLeastOne<{
    /** How many global uses are allowed. */
    global?: number;
    /** How many uses per user are allowed. */
    perUser?: number;
  }>,
) {
  useInhibitor(async (data) => await cooldownInhibitor(id, data, { ...config, time }));
}
