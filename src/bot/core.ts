import { GatewayIntents, katsura as init } from "../../deps.ts";
import env from "../utils/env.ts";
import { CommandsStore } from "./commands/mod.ts";

export const fileLoaderPaths = ["./src/bot/commands", "./src/bot/cron"];
export const fileLoaderExtensions = [".ts", ".tsx"];

export const bot = init({
  token: env.token,
  // Guilds - slash command propagation
  intents: GatewayIntents.Guilds,
  slashCommands: CommandsStore.slashCommands,
  messageContextCommamnds: CommandsStore.messageContextCommamnds,
  userContextCommands: CommandsStore.userContextCommands,
});
