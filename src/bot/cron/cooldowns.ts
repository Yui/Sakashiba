import { cooldowns } from "../inhibitors/cooldown.ts";
import { CronStore } from "./mod.ts";

CronStore.cooldowns = {
  // Every 5 minutes
  expression: "*/5 * * * *",
  handler: () => {
    const now = Date.now();
    // Cleanup expired cooldowns
    for (const [key, cd] of cooldowns) {
      if (cd.timestamp > now) cooldowns.delete(key);
    }
  },
};
