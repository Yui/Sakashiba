import { Op, TokenBucket } from "../../../deps.ts";
import { Reminder } from "../../db/reminders.ts";
import { DiscordColors, Time } from "../../utils/constants.ts";
import { logger } from "../../utils/logger.ts";
import { bot } from "../core.ts";
import { CronStore } from "./mod.ts";

async function handleReminder(reminder: Reminder) {
  await bucket.waitTake();
  try {
    const dmChannel = await bot.rest.getDmChannel(reminder.userid);
    await bot.rest.sendMessage(dmChannel.id, {
      embeds: [{
        type: "rich",
        title: "Reminder",
        description: reminder.note,
        footer: { text: "This message contains user generated content." },
        color: DiscordColors.blurple.int,
      }],
    });
  } catch (err) {
    // Ignore error when someones has DMs disabled
    if (!(Reflect.has(err, "code") && err.code == 50007)) {
      logger.error(err);
    }

    //await reminder.destroy(); // Not needed as of now. Add this if reminders will be loopable eventually???
  }
}

const bucket = new TokenBucket(5, Time.second, 5);

CronStore.reminders = {
  expression: "* * * * *", // every minute
  handler: async () => {
    const now = new Date(Date.now());
    const limit = new Date(now.getTime() - Time.second * 59);

    // Delete old reminders
    await Reminder.destroy({ where: { date: { [Op.lt]: limit } } });

    // Get current reminder
    const reminders = await Reminder.findAll({ where: { date: { [Op.between]: [limit, now] } } });

    // Delete current reminders
    if (reminders.length > 0) {
      await Reminder.destroy({ where: { id: { [Op.in]: reminders.map((r) => r.id) } } });
    }

    // Trigger
    reminders.forEach((r) => handleReminder(r));
  },
};
