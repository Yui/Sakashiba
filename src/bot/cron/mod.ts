import { Cron } from "../../../deps.ts";
import { logger } from "../../utils/logger.ts";
import { type PossiblyPromise } from "../../utils/types.ts";

export function startCron() {
  reloadCron();
}

export type CronJob = {
  expression: string;
  handler: () => void | Promise<void>;
};

/** Call this after reloading CronStore with fileloader */
export function reloadCron() {
  for (const k in CronStore) {
    if (CronJobs[k]) {
      CronJobs[k].stop();
      delete CronJobs[k];
    }

    const def = CronStore[k];
    CronJobs[k] = new Cron(def.expression, catchErrors(def.handler));
  }
}

export const CronJobs = {} as Record<string, Cron>;

export const CronStore = {} as Record<string, CronJob>;

// deno-lint-ignore no-explicit-any
function catchErrors<T extends (...args: any[]) => PossiblyPromise<void>>(fn: T) {
  return async (...args: Parameters<T>): Promise<void> => {
    try {
      const promise = fn(...args);
      if (promise instanceof Promise) await promise;
    } catch (error) {
      logger.error("Cron Error", error);
    }
  };
}
