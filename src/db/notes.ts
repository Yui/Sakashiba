import { Column, DataType, InferAttributes, InferCreationAttributes, Model, Table } from "../../deps.ts";

export const NoteSizeLimit = 1000;

@Table
export class Note extends Model<
  InferAttributes<Note>,
  InferCreationAttributes<Note>
> {
  @Column({
    type: DataType.STRING,
    primaryKey: true,
  })
  declare name: string;

  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
  })
  declare guildid: bigint;

  @Column({
    type: DataType.BIGINT,
  })
  declare userid: bigint;

  @Column(DataType.STRING(NoteSizeLimit))
  declare text: string;
}
