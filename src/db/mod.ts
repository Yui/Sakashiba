import { Sequelize } from "../../deps.ts";
import env from "../utils/env.ts";
import { logger } from "../utils/logger.ts";
import { CommandVersion } from "./commandVersion.ts";
import { Note } from "./notes.ts";
import { Reminder } from "./reminders.ts";
export { CommandVersion } from "./commandVersion.ts";

export const sequelize = new Sequelize(
  env.postgres,
  {
    dialect: "postgres",
    models: [CommandVersion, Reminder, Note],
    logging: (sql) => logger.psql(sql),
  },
);

export async function connectToDB() {
  await sequelize.authenticate();

  await sequelize.sync({
    alter: true,
  });
}
