import { Column, DataType, InferAttributes, InferCreationAttributes, Model, Table } from "../../deps.ts";

export const ReminderSizeLimit = 1000;

@Table
export class Reminder extends Model<
  InferAttributes<Reminder>,
  InferCreationAttributes<Reminder>
> {
  @Column({ // Sequelize needs some primary key
    type: DataType.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  })
  declare id?: bigint; // TODO: somehow make this increment per user id if possible, (each user would start with 1)

  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
  })
  declare userid: bigint;

  @Column(DataType.STRING(ReminderSizeLimit))
  declare note: string;

  @Column(DataType.DATE)
  declare date: Date;
}
