import { Column, DataType, InferAttributes, InferCreationAttributes, Model, Table } from "../../deps.ts";

@Table
export class CommandVersion extends Model<
  InferAttributes<CommandVersion>,
  InferCreationAttributes<CommandVersion>
> {
  @Column({ // Sequelize needs some primary key
    type: DataType.BIGINT,
    primaryKey: true,
  })
  declare id: bigint;
  @Column(DataType.INTEGER)
  declare version: number;
}
