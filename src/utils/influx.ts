import { InfluxDB, Point } from "../../deps.ts";
import env from "./env.ts";
import { logger } from "./logger.ts";

export let guildCount: Point | undefined;

export function enableInfluxMetrics() {
  if (!(env.influxurl && env.influxtoken && env.influxorg && env.influxbucket)) {
    throw new Error("Missing influx config.");
  }

  guildCount = new Point("guildcount");

  const writeApi = new InfluxDB({ url: env.influxurl, token: env.influxtoken }).getWriteApi(
    env.influxorg,
    env.influxbucket,
    "ms",
  );

  setInterval(async () => {
    writeApi.writePoints([guildCount!]);
    await writeApi.flush().catch(logger.error);
  }, env.influxflushinterval);
}

//guildCount.intField("value", count * getRandomArbitrary(1, 100));
//await writeApi.writePoint(guildCount);
