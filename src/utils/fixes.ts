import { fixTransformerCustomizers, snakelize } from "../../deps.ts";
import { bot } from "../bot/core.ts";

export function applyFixes() {
  // Fix some customizers being empty TODO: Remove this when solved.
  fixTransformerCustomizers(bot.transformers);

  // Fix dd app info path TODO: Remove this when alternative possible
  const baseSendRequest = bot.rest.sendRequest;
  bot.rest.sendRequest = (options) => {
    if (options.route === "https://discord.com/api/v10/applications/@me") {
      options.route = "https://discord.com/api/v10/oauth2/applications/@me";
    }
    return baseSendRequest(options);
  };

  // Snakelize json when form data is used
  const baseFetch = window.fetch;
  window.fetch = (url, data) => {
    const sUrl = url.toString();
    if (sUrl.includes("api/v10/webhooks/") || sUrl.includes("api/v10/interactions")) {
      if (data?.body && data.body instanceof FormData) {
        const jsonRaw = data.body.get("payload_json")?.toString();
        if (jsonRaw) {
          data.body.set("payload_json", JSON.stringify(snakelize(JSON.parse(jsonRaw))));
        }
      }
    }
    return baseFetch(url, data);
  };
}
