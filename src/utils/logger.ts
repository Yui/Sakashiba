import {
  bgBlue,
  bgRgb24,
  blue,
  bold,
  brightRed,
  createDiscordWebhookTransport,
  createLogger,
  createStreamConsoleTransport,
  cyan,
  dim,
  gray,
  italic,
  red,
  yellow,
} from "../../deps.ts";
import { env } from "./env.ts";

enum levels {
  debug,
  info,
  music,
  psql,
  warn,
  error,
  fatal,
}

const levelColors = new Map<
  levels,
  { text: (text: string) => string; embedColor?: number }
>([
  [
    levels.debug,
    {
      text: gray,
    },
  ],
  [
    levels.info,
    {
      text: cyan,
    },
  ],
  [
    levels.music,
    {
      text: (text) => bgRgb24(dim(text), { r: 251, g: 92, b: 92 }),
    },
  ],
  [
    levels.psql,
    {
      text: (text) => bgBlue(dim(text)),
    },
  ],

  [
    levels.warn,
    {
      text: yellow,
      embedColor: 16776960,
    },
  ],
  [
    levels.error,
    {
      text: red,
      embedColor: 15158332,
    },
  ],
  [
    levels.fatal,
    {
      text: (text: string) => bold(italic(brightRed(text))),
      embedColor: 10038562,
    },
  ],
]);

const transports = [];

const streamConsoleFormatter: Parameters<
  typeof createStreamConsoleTransport
>[0]["formatter"] = (data) => {
  const date = blue(
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
  );
  const level = levelColors.get(data.levelIndex)!.text(data.level);
  const fileName = data.location?.filename
    ? gray(
      // Hacky fix to remove the `#0`(Fileloader adds this) from filenames
      ` (${
        data.location.filename.slice(0, data.location.filename.indexOf("#"))
      } L${data.location.line} C${data.location.col})`,
    )
    : "";

  return [
    date,
    level,
    typeof data.message !== "string" ? Deno.inspect(data.message, { colors: true, depth: 10 }) : data.message,
    fileName,
    ...(data.extra?.map((x) => typeof x !== "string" ? Deno.inspect(x, { colors: true, depth: 10 }) : x) ?? []),
  ] as string[];
};

const streamOutConsoleTransport = createStreamConsoleTransport({
  levels: levels,
  std: "out",
  formatter: (data) => streamConsoleFormatter(data),
})
  .disable(levels.error)
  .disable(levels.fatal);

const streamErrConsoleTransport = createStreamConsoleTransport({
  levels,
  std: "err",
  formatter: (data) => streamConsoleFormatter(data),
})
  .disableAll()
  .enable(levels.error)
  .enable(levels.fatal);

transports.push(streamOutConsoleTransport, streamErrConsoleTransport);

if (env.loggerwebhook) {
  // Regex to remove ANSI Colors
  // Source: Sindre Sorhus - https://github.com/chalk/ansi-regex/blob/1b337add136eb520764634a328e2f6354398eee5/index.js#L2
  const removeANSIRegex =
    "[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))";

  const discordTransport = createDiscordWebhookTransport({
    levels,
    webhook: env.loggerwebhook,
    rateLimits: {
      respectLimits: true,
    },
    formatter: (data) => {
      const level = `**${data.levelIndex == levels.fatal ? `🧯🔥 ${data.level} 🧯🔥` : data.level}**`;
      const message = String(data.message).replaceAll(removeANSIRegex, "");
      const fileName = data.location?.filename
        ? `\`${data.location?.filename} L${data.location?.line} C${data.location?.col}\``
        : undefined;
      const color = levelColors.get(data.levelIndex)?.embedColor;

      const dataFormatStart = "```ts\n";
      const dataFormatEnd = "\n```";
      const description = `${message}${fileName ? "\n" + fileName : ""}`;
      const maxDataLength = 4096 -
        (dataFormatStart.length + dataFormatEnd.length + "\n".length +
          description.length);

      const formattedData = data.extra && data.extra.length > 0
        ? `${data.extra!.map((x) => Deno.inspect(x)).join(" ")}`
        : undefined;
      if (formattedData && formattedData?.length > maxDataLength) {
        formattedData.slice(0, maxDataLength);
      }

      return {
        embeds: [
          {
            title: level,
            type: "rich",
            color: color,
            fields: [
              {
                name: "message",
                value: message,
              },
              {
                name: "file",
                value: fileName ?? "Unknown",
              },
            ],
            description: formattedData ? `${dataFormatStart}${formattedData}${dataFormatEnd}` : undefined,
          },
        ],
      };
    },
  });

  discordTransport.logLevel = levels.warn;

  transports.push(discordTransport);
}

export const logger = await createLogger({
  transports: transports,
  levels: levels,
  trackPaths: ["fatal", "error", "debug", "warn"],
});

//if (env.prod) logger.disable(levels.debug);

if (env.loggersql) logger.enable(levels.psql);
else logger.disable(levels.psql);
