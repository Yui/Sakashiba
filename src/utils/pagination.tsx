import {
  Button,
  Camelize,
  collectComponent,
  DiscordEmbed,
  InteractionResponseTypes,
  MyInteractionResponse,
  Row,
} from "../../deps.ts";
import { bot } from "../bot/core.ts";
import { PossiblyPromise } from "./types.ts";

export function createStaticPagination(data: {
  embeds: Camelize<DiscordEmbed | DiscordEmbed[]>[];
}): {
  /** First response which you send to discord. */
  data: MyInteractionResponse;
  /** Promise of this pagination. */
  end: Promise<void>;
} {
  // deno-lint-ignore no-explicit-any
  let paginationEndPromiseControls: { resolve: () => void; reject: (reason?: any) => void };

  const paginationEndPromise = new Promise<void>((resolve, reject) =>
    paginationEndPromiseControls = { resolve, reject }
  );

  let currentIndex = 0;
  // Collector
  let prevButton = collectComponent("component");
  let nextButton = collectComponent("component");

  try {
    const currentEmbeds = data.embeds[currentIndex];

    // Return message to send to discord
    return {
      data: {
        type: "ChannelMessageWithSource",
        embeds: Array.isArray(currentEmbeds) ? currentEmbeds : [currentEmbeds],
        components: [
          <Row>
            <Button label="prev" customId={prevButton[1]} disabled={currentIndex == 0} />
            <Button label="next" customId={nextButton[1]} disabled={currentIndex == data.embeds.length - 1} />
          </Row>,
        ],
      },
      end: paginationEndPromise,
    };
  } finally {
    const loopThisFunction = () => {
      // Wait for collectors
      Promise.any([prevButton[0], nextButton[0]]).then(async (collected) => {
        // Handle index based on buttons
        if (collected.data?.customId) {
          if (collected.data.customId === prevButton[1]) currentIndex--;
          else if (collected.data.customId === nextButton[1]) currentIndex++;
        }

        // Redo collectors
        prevButton = collectComponent("component");
        nextButton = collectComponent("component");

        const currentEmbeds = data.embeds[currentIndex];

        // Edit message
        await bot.rest.sendInteractionResponse(collected.id, collected.token, {
          type: InteractionResponseTypes.UpdateMessage,
          data: {
            embeds: Array.isArray(currentEmbeds) ? currentEmbeds : [currentEmbeds],
            components: [
              <Row>
                <Button label="prev" customId={prevButton[1]} disabled={currentIndex == 0} />
                <Button label="next" customId={nextButton[1]} disabled={currentIndex == data.embeds.length - 1} />
              </Row>,
            ],
          },
        });

        // Wait for collectors again
        loopThisFunction();
      }).catch((err) => paginationEndPromiseControls.reject(err));
    };
    // First call
    loopThisFunction();
  }
}

export async function createDynamicPagination(data: {
  /** Return number of pages. */
  count: () => PossiblyPromise<number>;
  page: (pageIndex: number, pagesCount: number) => PossiblyPromise<{
    content?: string;
    embeds?: Camelize<DiscordEmbed> | Camelize<DiscordEmbed>[];
  }>;
  ephermal?: boolean;
}): Promise<{
  /** First response which you send to discord. */
  data: MyInteractionResponse;
  /** Promise of this pagination. */
  end: Promise<void>;
}> {
  const flags = data.ephermal ? 64 : undefined;
  // deno-lint-ignore no-explicit-any
  let paginationEndPromiseControls: { resolve: () => void; reject: (reason?: any) => void };

  const paginationEndPromise = new Promise<void>((resolve, reject) =>
    paginationEndPromiseControls = { resolve, reject }
  );

  let currentIndex = 0;
  // Collector
  let prevButton = collectComponent("component");
  let nextButton = collectComponent("component");

  try {
    const maxCount = await data.count();
    const currentPageData = await data.page(currentIndex, maxCount);

    // Return message to send to discord
    return {
      data: {
        type: "ChannelMessageWithSource",
        content: currentPageData.content,
        embeds: currentPageData.embeds
          ? Array.isArray(currentPageData.embeds) ? currentPageData.embeds : [currentPageData.embeds]
          : undefined,
        components: [
          <Row>
            <Button label="prev" customId={prevButton[1]} disabled={currentIndex == 0} />
            <Button label="next" customId={nextButton[1]} disabled={currentIndex == maxCount - 1} />
          </Row>,
        ],
        flags,
      },
      end: paginationEndPromise,
    };
  } finally {
    const loopThisFunction = () => {
      // Wait for collectors
      Promise.any([prevButton[0], nextButton[0]]).then(async (collected) => {
        // Handle index based on buttons
        if (collected.data?.customId) {
          if (collected.data.customId === prevButton[1]) currentIndex--;
          else if (collected.data.customId === nextButton[1]) currentIndex++;
        }

        // Redo collectors
        prevButton = collectComponent("component");
        nextButton = collectComponent("component");

        const maxCount = await data.count();
        const currentPageData = await data.page(currentIndex, maxCount);

        // Edit message
        await bot.rest.sendInteractionResponse(collected.id, collected.token, {
          type: InteractionResponseTypes.UpdateMessage,
          data: {
            content: currentPageData.content,
            embeds: currentPageData.embeds
              ? Array.isArray(currentPageData.embeds) ? currentPageData.embeds : [currentPageData.embeds]
              : undefined,
            components: [
              <Row>
                <Button label="prev" customId={prevButton[1]} disabled={currentIndex == 0} />
                <Button label="next" customId={nextButton[1]} disabled={currentIndex == maxCount - 1} />
              </Row>,
            ],
            flags,
          },
        });

        // Wait for collectors again
        loopThisFunction();
      }).catch((err) => paginationEndPromiseControls.reject(err));
    };
    // First call
    loopThisFunction();
  }
}
