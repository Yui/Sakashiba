/** Few special characters which are not easily visible/typed. */
export enum SpecialChars {
  /** Zero width space (Invisible character) */
  zws = "\u200B",
}

// List of Discord Colors - https://gist.github.com/thomasbnt/b6f455e2c7d743b796917fa3c205f812
/** Discord color values in hex format*/
export const DiscordColors = {
  default: {
    hex: "#000000",
    int: 0,
  },
  aqua: {
    hex: "#1ABC9C",
    int: 1752220,
  },
  darkAqua: {
    hex: "#11806A",
    int: 1146986,
  },
  green: {
    hex: "#2ECC71",
    int: 5763719,
  },
  darkGreen: {
    hex: "#1F8B4C",
    int: 2067276,
  },
  blue: {
    hex: "#3498DB",
    int: 3447003,
  },
  darkBlue: {
    hex: "#206694",
    int: 2123412,
  },
  purple: {
    hex: "#9B59B6",
    int: 10181046,
  },
  darkPurple: {
    hex: "#71368A",
    int: 7419530,
  },
  luminousVividPink: {
    hex: "#E91E63",
    int: 15277667,
  },
  darkVividPink: {
    hex: "#AD1457",
    int: 11342935,
  },
  gold: {
    hex: "#F1C40F",
    int: 15844367,
  },
  darkGold: {
    hex: "#C27C0E",
    int: 12745742,
  },
  orange: {
    hex: "#E67E22",
    int: 15105570,
  },
  darkOrange: {
    hex: "#A84300",
    int: 11027200,
  },
  red: {
    hex: "#E74C3C",
    int: 15548997,
  },
  darkRed: {
    hex: "#992D22",
    int: 10038562,
  },
  grey: {
    hex: "#95A5A6",
    int: 9807270,
  },
  darkGrey: {
    hex: "#979C9F",
    int: 9936031,
  },
  darkerGrey: {
    hex: "#7F8C8D",
    int: 8359053,
  },
  lightGrey: {
    hex: "#BCC0C0",
    int: 12370112,
  },
  navy: {
    hex: "#34495E",
    int: 3426654,
  },
  darkNavy: {
    hex: "#2C3E50",
    int: 2899536,
  },
  yellow: {
    hex: "#FFFF00",
    int: 16776960,
  },
  white: {
    hex: "#FFFFFF",
    int: 16777215,
  },
  greyple: {
    hex: "#99AAb5",
    int: 10070709,
  },
  black: { //
    hex: "#23272A",
    int: 2303786,
  },
  darkbutnotblack: {
    hex: "#2C2F33",
    int: 2895667,
  },
  notquiteblack: {
    hex: "#23272A",
    int: 2303786,
  },
  blurple: {
    hex: "#5865F2",
    int: 5793266,
  },
  fuchsia: {
    hex: "#EB459E",
    int: 15418782,
  },
} as const;

// Source: 2021 Discordeno - https://github.com/discordeno/template/blob/48165243c6053fb9568c8df4fc25fd1273a92c11/src/utils/constants/time.ts
export enum Time {
  month = 1000 * 60 * 60 * 27 * 30,
  week = 1000 * 60 * 60 * 24 * 7,
  day = 1000 * 60 * 60 * 24,
  hour = 1000 * 60 * 60,
  minute = 1000 * 60,
  second = 1000,
}

export const Regexes = {
  /** https://www.sapphirejs.dev/docs/Guide/utilities/Discord_Utilities/UsefulRegexes#snowflake-regex */
  snowflake: /^(?<id>\d{17,20})$/,
  steamId64: /^[0-9]{17}$/,
};
