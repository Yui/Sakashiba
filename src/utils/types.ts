export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;
export type PossiblyPromise<T> = T | Promise<T>;
export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> & U[keyof U];
/** https://github.com/microsoft/TypeScript/issues/10421#issuecomment-804742331 */
// deno-lint-ignore no-explicit-any
export function assumeAs<T>(_val: any): asserts _val is T {}
