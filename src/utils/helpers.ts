export function snowflakeToTimestamp(id: string | bigint) {
  return Number(BigInt(id) / 4194304n + 1420070400000n);
}

export const DISCORD_TIME_FORMATS = {
  "Short Time": "t",
  "Long Time": "T",
  "Short Date": "d",
  "Long Date": "D",
  "Short Date/Time": "f",
  "Long Date/Time": "F",
  "Relative Time": "R",
};

export type DISCORD_TIME_TYPES = keyof typeof DISCORD_TIME_FORMATS;

/**
 * Creates a discord timestamp formatted string.
 *
 * `Date` can be unix timestamp (`number`) or `Date`
 *
 * **Formats**:
 *
 * **Short Time** - 16:20
 *
 * **Long Time** - 16:20:30
 *
 * **Short Date** - 20/04/2021 ( Default format )
 *
 * **Long Date** - 20 April 2021
 *
 * **Short Date/Time** - 20 April 2021 16:20
 *
 * **Long Date/Time** - Tuesday, 20 April 2021 16:20
 *
 * **Relative Time** - 2 months ago
 */

export function dateToDiscordTimestamp(
  date: Date | number,
  format?: DISCORD_TIME_TYPES,
): string {
  const value = date instanceof Date ? Math.floor(date.getTime() / 1000) : date;

  return `<t:${value}${format ? `:${DISCORD_TIME_FORMATS[format]}` : ""}>`;
}

// deno-lint-ignore no-explicit-any
function* filter<T extends any[]>(
  array: T,
  condition: (value: T[number], index: number, array: T) => boolean,
  maxSize: number,
) {
  if (!maxSize || maxSize > array.length) {
    maxSize = array.length;
  }
  let count = 0;
  let i = 0;
  while (count < maxSize && i < array.length) {
    if (condition(array[i], i, array)) {
      yield array[i];
      count++;
    }
    i++;
  }
}

// deno-lint-ignore no-explicit-any
export function filterArrayWithLimit<T extends any[]>(
  array: T,
  condition: (value: T[number], index: number, array: T) => boolean,
  maxSize: number,
): T {
  return Array.from(filter(array, condition, maxSize)) as T;
}
