import { load } from "../../deps.ts";

export const env = await load({
  token: /[M-Z][A-Za-z\d]{23}\.[\w-]{6}\.[\w-]{27}/,
  botid: {
    type: BigInt,
    optional: true,
  },
  testguildid: {
    type: BigInt,
    optional: true,
  },
  postgres: String,
  loggerwebhook: {
    type: /^.*(discord|discordapp)\.com\/api\/webhooks\/([^\/]+)\/([^\/]+)/,
    optional: true,
  },
  slashcommandversion: {
    type: Number,
    default: 1,
  },
  loggersql: {
    type: Boolean,
    default: false,
  },
  dev: {
    type: Boolean,
    default: true,
  },
  influxenabled: {
    type: Boolean,
    default: false,
  },
  influxurl: {
    type: String,
    optional: true,
  },
  influxtoken: {
    type: String,
    optional: true,
  },
  influxorg: {
    type: String,
    optional: true,
  },
  influxbucket: {
    type: String,
    optional: true,
  },
  influxflushinterval: {
    type: Number,
    default: 60000, // 1 minute
  },
}, {
  merge: true,
  export: false,
});

export default env;
